import { config } from "dotenv"
config()

import { CONTROL_CHAR } from "modules/upload-tracking/constants"
import {
  Bot,
  matchPrefixes,
  ConsoleLoggingService,
  CommandErrorService,
  CommandGroup,
  matchAll,
  matchChannelTypes
} from "@enitoni/gears"

import * as youtubeCommands from "modules/youtube/commands"
import * as uploadTrackingCommands from "modules/upload-tracking/commands"

import * as uploadTrackingServices from "modules/upload-tracking/services"
import { unknownCommand, restartCommand } from "modules/core/commands"
import { resolveRootPermission } from "modules/core/resolvers"

const group = new CommandGroup({
  matcher: matchAll([
    matchPrefixes([CONTROL_CHAR]),
    matchChannelTypes(["text"])
  ]),
  resolvers: [resolveRootPermission],
  commands: [
    ...Object.values(youtubeCommands),
    ...Object.values(uploadTrackingCommands),
    restartCommand,
    unknownCommand
  ]
})

const bot = new Bot({
  token: process.env.DISCORD_API_KEY as string,
  services: [
    ConsoleLoggingService,
    CommandErrorService,
    ...Object.values(uploadTrackingServices)
  ],
  commands: [group],
  metadata: {
    name: "Seymour"
  }
})

bot.start()
