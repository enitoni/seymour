import { Command, matchPrefixes } from "@enitoni/gears";

export const restartCommand = new Command({
  matcher: matchPrefixes(["restart"]),
  action: async (context) => {
    const { message, bot } = context

    await message.channel.send("Restarting, see you soon!")
    await bot.client.user.setStatus("invisible")

    process.exit()
  }
})