import { Command, matchAlways, CommandGroup } from "@enitoni/gears";
import { RichEmbed } from "discord.js";

export const unknownCommand = new Command({
  matcher: matchAlways(),
  action: async (context, issuer) => {
    const group = issuer as CommandGroup
    const { message } = context

    const commands = []

    for (const command of group.commands) {
      const { hidden, description, usage } = command.metadata

      if (!hidden) {
        const line = `**${usage}**\n${description}`
        commands.push(line)
      }
    }

    const embed = new RichEmbed()

    embed.setTitle("Available commands")
    embed.setDescription(commands.join("\n\n"))

    message.channel.send({ embed })
  }
})