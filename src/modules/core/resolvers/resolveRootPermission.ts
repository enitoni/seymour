import { PermissionResolver, Context, PermissionError } from "@enitoni/gears";
import { PERMISSION_WHITELIST } from "modules/core/constants";

export const resolveRootPermission: PermissionResolver = async (context: Context) => {
  const { message } = context

  const hasPermission = PERMISSION_WHITELIST.includes(message.author.id)
  if (!hasPermission) throw new PermissionError("You must be whitelisted.")
}