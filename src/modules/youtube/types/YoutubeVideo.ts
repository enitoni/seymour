export interface YoutubeVideo {
  id: string
  title: string
  description: string
  publishedAt: string
  thumbnail: string
}
