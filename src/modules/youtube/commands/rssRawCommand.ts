import { Command, matchPrefixes } from "@enitoni/gears"
import { getLatestFeedVideo } from "modules/youtube/actions/getLatestFeedVideo"
import { RichEmbed } from "discord.js"
import { CONTROL_CHAR } from "modules/upload-tracking/constants"

export const rssRawCommand = new Command({
  metadata: {
    name: "Raw",
    description:
      "Outputs a raw JSON result of the most recent partial RSS feed video.",
    usage: `${CONTROL_CHAR}rss-raw`
  },
  matcher: matchPrefixes(["rss-raw"]),
  action: async context => {
    const { message } = context
    const video = await getLatestFeedVideo()

    if (!video) return message.channel.send("No video was found.")

    const stringified = JSON.stringify(video, null, 2)

    const embed = new RichEmbed({
      title: "Partial feed video",
      description: "```json\n" + stringified + "\n```"
    })

    return message.channel.send({ embed })
  }
})
