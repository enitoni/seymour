import { Command, matchPrefixes } from "@enitoni/gears"
import { CONTROL_CHAR } from "modules/upload-tracking/constants"
import { getLatestFeedVideo, getVideoById } from "modules/youtube/actions"

export const rawCommand = new Command({
  metadata: {
    name: "Raw",
    description: "Outputs a raw JSON result of the latest video.",
    usage: `${CONTROL_CHAR}raw`
  },
  matcher: matchPrefixes(["raw"]),
  action: async context => {
    const { message } = context

    const partial = await getLatestFeedVideo()
    const video = await getVideoById(partial!.id)

    if (!video) return message.channel.send("No video was found.")

    const stringified = JSON.stringify(video, null, 2)
    return message.channel.send("```json\n" + stringified + "\n```")
  }
})
