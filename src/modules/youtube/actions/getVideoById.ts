import * as Request from "request-promise-native"

import { YOUTUBE_API_URL } from "modules/youtube/constants"
import { YoutubeVideo } from "modules/youtube/types"
const { YOUTUBE_API_KEY } = process.env

export const getVideoById = async (
  id: string
): Promise<YoutubeVideo | null> => {
  const videoOptions = {
    key: YOUTUBE_API_KEY,
    maxResults: 1,
    part: "snippet",
    id
  }

  const videoResult = await Request.get(`${YOUTUBE_API_URL}/videos`, {
    json: true,
    qs: videoOptions
  })

  const [video] = videoResult.items

  if (!video) return null

  const safeVideo = {
    id: video.id,
    title: video.snippet.title,
    description: video.snippet.description,
    publishedAt: video.snippet.publishedAt,
    thumbnail: video.snippet.thumbnails.maxres.url
  }

  return safeVideo
}
