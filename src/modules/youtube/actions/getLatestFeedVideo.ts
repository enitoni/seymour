import RSSParser from "rss-parser"
import { RSS_FEED } from "modules/youtube/constants"

const parser = new RSSParser({
  customFields: {
    item: ["yt:videoId"]
  }
})

export interface PartialFeedVideo {
  id: string
  publishedAt: string
}

export const getLatestFeedVideo = async (): Promise<
  PartialFeedVideo | undefined
> => {
  const feed = await parser.parseURL(RSS_FEED)
  if (!feed.items || feed.items.length === 0) return

  const latest = feed.items[0]

  return {
    id: latest["yt:videoId"],
    publishedAt: latest.pubDate!
  }
}
