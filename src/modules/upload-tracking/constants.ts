const { YOUTUBE_CHANNEL_ID } = process.env;

export const CONTROL_CHAR = "§";
export const CONTROL_REGEX = `${CONTROL_CHAR}(.?)+: `;
export const CONTROL_REPLACE_REGEX = `[${CONTROL_CHAR}|:| ]`;

export const TAG_TYPES: Record<string, { args: string[]; rest?: boolean }> = {
  review: {
    args: ["editor", "skinName"],
    rest: true
  },
  guest: {
    args: ["editor", "skinName"],
    rest: true
  },
  announcement: {
    args: ["narrator", "editor"]
  },
  livestream: {
    args: ["streamer", "game"]
  }
};

export const CHANNEL_ID = "490055523366862876";
export const ERROR_CHANNEL_ID = "458245595023409152";
export const ROLE_ID = "489097325117964299";

export const REVIEW_EMBED_COLOR = 0x00c8ff;
export const ANNOUNCEMENT_EMBED_COLOR = 0xff5ace;
export const LIVESTREAM_EMBED_COLOR = 0x9e85f5;

export const EMBED_LOGO = "https://cbullet.us/Nn1d.png";
export const EMBED_AUTHOR_URL = `https://www.youtube.com/channel/${YOUTUBE_CHANNEL_ID}`;

export const LIVESTREAM_BLURBS = [
  "Live in 5... 4... 3... 2...",
  "You thought this was a video. HA!",
  "Oh frig they really bouta do it.",
  "black guy: hey",
  "It's the game's fault, not CBullet's.",
  "New video but it gets procedurally generated as you watch it.",
  "It's like a regular video but with way less effort.",
  "Filler conte- I mean livestream.",
  "w̳̲͞e̕'̮̮́ve̹͝ ̱́ͅt̨̜̰r͉̞͎a̩̤̻p̳̱ͅp̫̥e͏d̥̞͔ ̦̬́t̘̗hͅe̡̼̭ ҉̣̞̺s̰̩̩ơ̞̳u͟l͓̩̼ ̲̞̥o҉͓̟͍f̤̲͓ ̫̯͟a̹͖͔ ̡̰̩s͎͓͜t̷̬ͅa̖̟͡f̶̙f ͇͖̰m̤e͖ͅͅm͈̫̘b̘̱͔e̻̩͢r̠̥͙ ̶t͏̬̼o ̜̳͜b̺̼̯r͖̬͟i̵ͅń̲̺g̘͇ y͖̯͓o̧̳̤u͖͢ ̱͙̦t͟h͖̭͜i̡͉̻ş̠ͅ ̦̱̣t̻r̘a̟̼̤nsmission"
];
