import { Command, matchPrefixes, stringType, ArgumentList } from "@enitoni/gears";
import { TextChannel } from "discord.js";

import { parseDescData, getNotificationEmbed } from "modules/upload-tracking/helpers";
import { EXAMPLE_VIDEO } from "modules/youtube/constants";
import { CONTROL_CHAR } from "modules/upload-tracking/constants";

const argList = new ArgumentList({
  args: [
    {
      name: "type",
      required: true,
      types: [ stringType ]
    }
  ],
  restArg: {
    name: "value",
    types: [ stringType ]
  }
})

export const createEmbedCommand = new Command({
  metadata: {
    name: "Create embed",
    description: "Creates an embed from description values",
    usage: `${CONTROL_CHAR}create-embed ${argList.getUsage()}`,
  },
  matcher: matchPrefixes(["create-embed"]),
  action: async (context) => {
    const { message } = context
    const [ type, ...values ] = argList.parse(context)
    const data = parseDescData({ type, values })

    if (!data) return message.channel.send("Invalid data.")

    const embed = getNotificationEmbed({
      video: EXAMPLE_VIDEO,
      channel: message.channel as TextChannel,
      data,
    })

    return message.channel.send({ embed })
  }
})