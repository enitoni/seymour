import { Command, matchPrefixes } from "@enitoni/gears";
import { UploadPollingService } from "modules/upload-tracking/services";
import { CONTROL_CHAR } from "modules/upload-tracking/constants";

export const pollCommand = new Command({
  metadata: {
    name: "Force poll",
    description: "Forces the bot to poll for new videos.",
    usage: `${CONTROL_CHAR}poll`,
  },
  matcher: matchPrefixes(["poll"]),
  action: async (context) => {
    const { bot, message } = context
    const service = bot.getService(UploadPollingService)

    await message.channel.send("Forcing a poll...")
    service.poll()
  }
})