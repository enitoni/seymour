import { Command, matchPrefixes, ArgumentList, stringType } from "@enitoni/gears";
import { CONTROL_CHAR } from "modules/upload-tracking/constants";

const argList = new ArgumentList({
  args: [
    {
      name: "type",
      required: true,
      types: [ stringType ]
    }
  ],
  restArg: {
    name: "value",
    types: [ stringType ]
  }
})

export const createTagCommand = new Command({
  metadata: {
    name: "Create tag",
    description: "Create a tag the bot will recognize in a description from values separated by space.",
    usage: `${CONTROL_CHAR}create-tag ${argList.getUsage()}`
  },
  matcher: matchPrefixes(["create-tag"]),
  action: async (context) => {
    const { message } = context

    const args = argList.parse(context)
    const [ type, ...values ] = args

    if (values.length === 0) {
      return message.channel.send("At least one value is required.")
    }

    const result = `${CONTROL_CHAR}${type}: ${values.join("|")}`
    message.channel.send(`\`${result}\``)
  }
})