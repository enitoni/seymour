import { TAG_TYPES } from "modules/upload-tracking/constants";

export interface DescriptionData {
  type: keyof typeof TAG_TYPES,
  values: string[]
}
