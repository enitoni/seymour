


export const humanizeReviewerList = (items: string[]) => {
  if (items.length === 1) return `${items[0]}`

  if (items.length === 2) return `${items[0]} and ${items[1]}`

  const [ lastReviewer, ...reversedReviewers ] = items.reverse()
  const reviewers = reversedReviewers.reverse()

  return `${reviewers.join(", ")}, and ${lastReviewer}`
}