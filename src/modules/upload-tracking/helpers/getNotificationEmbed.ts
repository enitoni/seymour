import { getRandomItem } from "common/lang/array"
import { RichEmbed, TextChannel } from "discord.js"
import {
  ANNOUNCEMENT_EMBED_COLOR,
  EMBED_AUTHOR_URL,
  EMBED_LOGO,
  LIVESTREAM_BLURBS,
  LIVESTREAM_EMBED_COLOR,
  REVIEW_EMBED_COLOR
} from "modules/upload-tracking/constants"
import {
  getVideoStaffMemberString,
  humanizeReviewerList,
  ParsedDescriptionData
} from "modules/upload-tracking/helpers"
import { getYouTubeLinkById } from "modules/youtube/helpers"
import { YoutubeVideo } from "modules/youtube/types"

export interface Options {
  data: ParsedDescriptionData
  video: YoutubeVideo
  channel: TextChannel
}

export const getNotificationEmbed = (options: Options) => {
  const { data, video, channel } = options
  const { type, reviewers } = data

  const parsedReviewers = reviewers.map(reviewer =>
    getVideoStaffMemberString(reviewer, channel)
  )
  const embed = new RichEmbed()

  embed.setAuthor("skinhouse", EMBED_LOGO, EMBED_AUTHOR_URL)
  embed.setDescription(getYouTubeLinkById(video.id))
  embed.setImage(video.thumbnail)

  if (type === "review") {
    const { skinName, editor } = data
    const parsedEditor = getVideoStaffMemberString(editor, channel)

    embed.setColor(REVIEW_EMBED_COLOR)
    embed.setTitle("A new review has been uploaded!")

    embed.addField("Reviewed by", humanizeReviewerList(parsedReviewers))
    embed.addField("Editor", parsedEditor, true)
    embed.addField("Skin", skinName, true)
  }

  if (type === "announcement") {
    const { narrator, editor } = data

    const parsedEditor = getVideoStaffMemberString(editor, channel)
    const parsedNarrator = getVideoStaffMemberString(narrator, channel)

    embed.setColor(ANNOUNCEMENT_EMBED_COLOR)
    embed.setTitle("New announcement!")

    embed.addField("Narrator", parsedNarrator, true)
    embed.addField("Editor", parsedEditor, true)
  }

  if (type === "livestream") {
    const { streamer, game } = data

    const parsedStreamer = getVideoStaffMemberString(streamer, channel)

    embed.setColor(LIVESTREAM_EMBED_COLOR)
    embed.setTitle(getRandomItem(LIVESTREAM_BLURBS))

    embed.setDescription(
      `${parsedStreamer} is live with ${game}!\n ${getYouTubeLinkById(
        video.id
      )}`
    )
  }

  return embed
}
