import { CONTROL_REGEX, CONTROL_REPLACE_REGEX } from "modules/upload-tracking/constants";
import { DescriptionData } from "modules/upload-tracking/types";

export const getDataFromDesc = (desc: string): DescriptionData | null => {
  const regex = RegExp(CONTROL_REGEX)
  const replaceRegex = RegExp(CONTROL_REPLACE_REGEX, "g")

  const match = regex.exec(desc)
  if (!match) return null

  const delimiter = match[0]
  const type = delimiter.replace(replaceRegex, "")

  const [, valueString] = desc.split(delimiter)
  const values = valueString.split("|")

  return { type, values }
}