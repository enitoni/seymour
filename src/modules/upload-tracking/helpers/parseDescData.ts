import { TAG_TYPES } from "modules/upload-tracking/constants";
import { DescriptionData } from "modules/upload-tracking/types";

export type ParsedDescriptionData = {
  type: keyof typeof TAG_TYPES;
  reviewers: string[];
} & { [key: string]: string };

export const parseDescData = (
  data: DescriptionData
): ParsedDescriptionData | null => {
  const result: any = {};

  const format = TAG_TYPES[data.type];
  if (!format) return null;

  const hasMissingValues = format.args.length + 1 > data.values.length;
  if (hasMissingValues && format.rest) return null;

  for (const [index, key] of format.args.entries()) {
    const value = data.values[index];
    if (!value) return null;

    result[key] = value;
  }

  result.type = data.type;
  result.reviewers = data.values.slice(format.args.length);

  return result;
};
