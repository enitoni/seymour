import { TextChannel } from "discord.js";
import { ROLE_ID } from "modules/upload-tracking/constants";

export const getVideoStaffMemberString = (name: string, channel: TextChannel) => {
  const member = channel.guild.members.find(member => (
    (
      member.displayName.toLowerCase().includes(name.toLowerCase()) ||
      member.user.username.toLowerCase().includes(name.toLowerCase())
    )
    && member.roles.has(ROLE_ID)
  ))

  return member ? member.toString() : name
}