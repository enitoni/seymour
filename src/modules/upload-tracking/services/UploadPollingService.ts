import * as date from "date-fns"

import { StoredService } from "@enitoni/gears"
import { TextChannel, Channel } from "discord.js"
import { bind } from "decko"

import { getLatestFeedVideo, getVideoById } from "modules/youtube/actions"
import { getYouTubeLinkById } from "modules/youtube/helpers"

import { CHANNEL_ID, ERROR_CHANNEL_ID } from "modules/upload-tracking/constants"
import {
  getDataFromDesc,
  parseDescData,
  getNotificationEmbed,
  ParsedDescriptionData
} from "modules/upload-tracking/helpers"
import { YoutubeVideo } from "modules/youtube/types"
import { CBULLET, ENITONI } from "modules/youtube/constants"

interface StoredState {
  lastChecked: string
  lastVideo?: YoutubeVideo
}

const findTextChannelById = (id: string) => (channel: Channel) =>
  channel.id === id && channel.type === "text"

export class UploadPollingService extends StoredService<StoredState> {
  private channel!: TextChannel
  private errorChannel!: TextChannel

  private interval: any

  public getDefaultStoredState() {
    return {
      lastChecked: new Date().toISOString()
    }
  }

  protected async serviceDidStart() {
    const { client } = this.bot

    this.channel = client.channels.find(
      findTextChannelById(CHANNEL_ID)
    ) as TextChannel
    this.errorChannel = client.channels.find(
      findTextChannelById(ERROR_CHANNEL_ID)
    ) as TextChannel

    this.interval = setInterval(this.poll, 60000)
  }

  protected async serviceDidStop() {
    clearInterval(this.interval)
  }

  @bind
  public async poll() {
    const partial = await getLatestFeedVideo()

    if (partial) {
      const lastChecked = new Date(this.storedState.lastChecked)
      const newDate = new Date(partial.publishedAt)

      if (date.isAfter(newDate, lastChecked)) {
        this.handleNew(partial.id)
      }
    }
  }

  public async handleNew(id: string) {
    const video = await getVideoById(id)

    if (!video) {
      return this.errorChannel.send(
        `${ENITONI} Error: Video with id ${id} not found in YouTube API.`
      )
    }

    const descriptionData = getDataFromDesc(video.description)
    if (!descriptionData) return

    const parsedDescData = parseDescData(descriptionData)
    if (!parsedDescData) {
      return this.errorChannel.send(
        `${CBULLET} Incorrect tag data in ${getYouTubeLinkById(video.id)}`
      )
    }

    await this.setStoredState({ lastVideo: video, lastChecked: video.publishedAt })
    await this.notify(parsedDescData, video)
  }

  @bind
  public async notify(data: ParsedDescriptionData, video: YoutubeVideo) {
    const embed = getNotificationEmbed({
      data,
      video,
      channel: this.channel
    })

    await this.channel.send("<@&490052699270676480>", { embed })
  }

  public notifyError(video: YoutubeVideo) {
    this.errorChannel.send(
      `${CBULLET} Incorrect tag data in ${getYouTubeLinkById(video.id)}`
    )
  }
}
